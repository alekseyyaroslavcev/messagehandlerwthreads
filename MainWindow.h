#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();

private slots:
    void on_pbStart_clicked();

public slots:
    void addMsgToLog(QString msg);

signals:
    void startWorkers();

private:
    Ui::MainWindow* ui;
};

#endif // MAINWINDOW_H
