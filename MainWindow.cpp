#include "MainWindow.h"
#include "ui_MainWindow.h"

MainWindow::MainWindow(QWidget* parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbStart_clicked()
{
    ui->pbStart->setEnabled(false);
    emit startWorkers();
}

void MainWindow::addMsgToLog(QString msg)
{
    ui->log->insertPlainText(msg);
}
