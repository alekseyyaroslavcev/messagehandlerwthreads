#include "MainWindow.h"
#include <QApplication>
#include <QThread>
#include "Worker.h"
#include <iostream>

MainWindow* wPtr;

void messageHandler(QtMsgType type, const QMessageLogContext& context, const QString& msg);

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);
    MainWindow w;
    wPtr = &w;

    QThread thA;
    Worker a;
    a.moveToThread(&thA);
    a.setName("Object A ");
    QObject::connect(&w, &MainWindow::startWorkers, &a, &Worker::start);
    thA.start();

    QThread thB;
    Worker b;
    b.moveToThread(&thB);
    b.setName("Object B ");
    QObject::connect(&w, &MainWindow::startWorkers, &b, &Worker::start);
    thB.start();

    qInstallMessageHandler(messageHandler);

    w.show();

    return app.exec();
}

void messageHandler(QtMsgType /*type*/, const QMessageLogContext& /*context*/, const QString& msg)
{
    QMetaObject::invokeMethod(wPtr, "addMsgToLog", Qt::QueuedConnection, Q_ARG(QString,
                                                                               msg + QString("started\n")));
    QThread::sleep(10);
    QMetaObject::invokeMethod(wPtr, "addMsgToLog", Qt::QueuedConnection, Q_ARG(QString,
                                                                               msg + QString("stoped\n")));
}

