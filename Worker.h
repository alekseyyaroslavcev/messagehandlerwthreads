#ifndef WORKER_H
#define WORKER_H

#include <QObject>

class Worker : public QObject
{
    Q_OBJECT

    QString _name;
public:
    explicit Worker(QObject* parent = nullptr);

    void setName(QString name);

signals:

public slots:
    void start();
};

#endif // WORKER_H
