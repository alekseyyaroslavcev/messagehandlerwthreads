#include "Worker.h"
#include <QThread>

Worker::Worker(QObject* parent) : QObject(parent)
{

}

void Worker::setName(QString name)
{
    _name = name;
    thread()->setObjectName(_name);
}

void Worker::start()
{
    qInfo(_name.toStdString().c_str());
}
